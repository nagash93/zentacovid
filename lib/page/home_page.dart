import 'package:flutter/material.dart';
import 'package:zenta_covid/ultil/responsive.dart';
import 'package:zenta_covid/widget/card_virus.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final responsive = Responsive(context);

    return Scaffold(
        backgroundColor: Color(0xFFF2F4F5),
      body:SafeArea(child: Column(

        children: <Widget>[
         Container(
           height: responsive.wp(45),
           child:  ListView(
             scrollDirection: Axis.horizontal,
             children: <Widget>[

               CardVirus(123),
               CardVirus(123),
               CardVirus(123),

             ],
           ),
         )
        ],
      ))
    );
  }
}
