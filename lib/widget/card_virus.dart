import 'package:flutter/material.dart';
import 'package:zenta_covid/ultil/responsive.dart';

class CardVirus extends StatelessWidget {

  int virus;


  CardVirus(this.virus);

  @override
  Widget build(BuildContext context) {

    final responsive = Responsive(context);
    return Container(
      width: responsive.wp(40),
      height: responsive.wp(40),
      margin: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20)
      ),
      child:Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.bug_report,
            size: 40,
            color: Colors.green,
          ),
          Text(
            virus.toString(),
            style: TextStyle(
              fontSize: 30,
            ),
          )
        ],
      ),
    );
  }
}
